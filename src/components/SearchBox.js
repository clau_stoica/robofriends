import React from 'react';

const SearchBox = ({ searchChange, text }) => {
  return (
    <div className='pa2'>
      <input
        className='pa3 ba b--green bg-lightest-blue'
        type='search'
        placeholder='search robots'
        onChange={searchChange}
        value={text}
      />
    </div>
  );
};

export default SearchBox;