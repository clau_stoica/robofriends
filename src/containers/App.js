import React, { Component } from 'react';
import CardList from '../components/CardList';
import SearchBox from '../components/SearchBox';
import Scroll from '../components/Scroll';
import './App.css';
import { setSearchField, requestRobots } from '../actions'
import { connect } from 'react-redux'

// is used for selecting the part of the data from the store that the connected component needs
const mapStateToProps = state => {
    return {
        searchField: state.searchRobots.searchField,
        robots: state.requestRobots.robots,
        isPending: state.requestRobots.isPending,
        error: state.requestRobots.error,
    }
};

// is used for dispatching actions to the store
// lets you create functions that dispatch when called, and pass those functions as props to your component
const mapDispatchToProps = (dispatch) => {
  return {
    onSearchChange: (event) => dispatch(setSearchField(event.target.value)),
    onCardClick: (event) => {
        dispatch(setSearchField(event.currentTarget.getElementsByTagName('h2')[0].textContent))
    },
    onRequestRobots: () => dispatch(requestRobots())
  }
};

class App extends Component {
  componentDidMount() {
    this.props.onRequestRobots();
  }

  render() {
    const { searchField, onSearchChange, onCardClick, isPending, robots } = this.props;
    const filteredRobots = robots.filter(robot => {
      return robot.name.toLowerCase().includes(searchField.toLowerCase());
    });

    return isPending ?
      <h1>Loading</h1> :
      (
        <div className='tc'>
          <h1 className='f1'>RoboFriends</h1>
          <SearchBox searchChange={onSearchChange} text={searchField}/>
          <Scroll>
            <CardList robots={filteredRobots} onCardClick={onCardClick} />
          </Scroll>
        </div>
      );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);